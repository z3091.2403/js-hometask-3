# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: 
Можно сохранить ссылку на this во внутреннюю переменную
метод bind связывает функцию и нужный контекст (например объект)/  Контекст присываивается, но вызывать надо отдельно
call - присваивается контекст и вызывается метод сразу
apply  - это как call, только параметры в виде массива передаются
еще стрелочную функцию наверное можно сюда же - ведь с еепомощью ссылаемся на внешнее окружение
#### 2. Что такое стрелочная функция?
> Ответ: функция , которая не имеет своего this и берет его из своего окружения.
#### 3. Приведите свой пример конструктора. 
```js
// Ответ:

function Dog(name, breed) {
  this.name = name;
  this.breed = breed;
}
const myDog = new Dog ('Marta', 'Samoed')
```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
// 1. сохраняем ссылку на контекст
  const person = {
    name: 'Nikita',
    sayHello: function() {
      const that = this;
      setTimeout(function() {
          console.log(that.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();

//2. Bind
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }.bind(this), 1000);
    }
  }

  person.sayHello()

  //3. стрелочная функция
    const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(()=> {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
  ```
## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
